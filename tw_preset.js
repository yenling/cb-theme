module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: 'rgb(121, 199, 191)',
          light: 'rgb(214, 238, 236)',
          heavy: 'rgb(72, 165, 156)',
        },
        accent: {
          DEFAULT: 'rgb(231, 221, 76)',
          light: 'rgb(248, 243, 205)',
          heavy: 'rgb(212, 183, 0)'
        },
      },

      zIndex: {
        modal: 1000
      },
    },
  },
  // preset 放不了 shortcuts
  // shortcuts: {}
}
